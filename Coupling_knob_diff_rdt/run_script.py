import logging
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os.path
import os
import tfs
import sys
from cpymad.madx import Madx
from optics_functions.coupling import coupling_via_cmatrix,closest_tune_approach,coupling_via_rdts
from optics_functions.utils import split_complex_columns

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")


df_twiss = tfs.read('remove.tfs',index='NAME')
df_coupling = coupling_via_cmatrix(df_twiss)
df_coupling['NAMES'] = df_twiss.index
df_twiss['NAMES'] = df_twiss.index



diff_rdt_real = np.real(df_coupling['F1001'][np.where(df_coupling['NAMES']=='IP.2')[0][0]])
diff_rdt_imag = np.imag(df_coupling['F1001'][np.where(df_coupling['NAMES']=='IP.2')[0][0]])
sum_rdt_real = np.real(df_coupling['F1010'][np.where(df_coupling['NAMES']=='IP.2')[0][0]])
sum_rdt_imag = np.imag(df_coupling['F1010'][np.where(df_coupling['NAMES']=='IP.2')[0][0]])


if os.path.exists('ip_parameters.madx'):
    os.remove('ip_parameters.madx')
    f = open("ip_parameters.madx","w")
    
    f.write(f'diff_rdt_real={diff_rdt_real};\n')
    f.write(f'diff_rdt_imag={diff_rdt_imag};\n')
    f.write(f'sum_rdt_real={sum_rdt_real};\n')
    f.write(f'sum_rdt_imag={sum_rdt_imag};\n')
    f.close()
else:
    f = open("ip_parameters.madx","w")

    f.write(f'diff_rdt_real={diff_rdt_real};\n')
    f.write(f'diff_rdt_imag={diff_rdt_imag};\n')
    f.write(f'sum_rdt_real={sum_rdt_real};\n')
    f.write(f'sum_rdt_imag={sum_rdt_imag};\n')
    f.close()


       
        



