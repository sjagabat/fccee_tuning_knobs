import logging
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import tfs
import sys
from cpymad.madx import Madx
from optics_functions.coupling import coupling_via_cmatrix, closest_tune_approach,coupling_via_rdts
from optics_functions.utils import split_complex_columns
from optics_functions.rdt import calculate_rdts, generator, jklm2str
from optics_functions.utils import prepare_twiss_dataframe, split_complex_columns

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")


############## Definition to run the madx and provide the final twiss in every iteration of knob values
def run_mad(num1,num2):
    madx = Madx(stdout=True)
    madx.option(echo=True)
    madx.input('''

    option,update_from_parent=true; // new option in mad-x as of 2/2019


    !--------------------------------------------------------------
    ! Lattice selection and beam parameters
    !--------------------------------------------------------------

    CALL, FILE='Z_sequence.seq';


    pbeam =   45.6;
    EXbeam = 7.1e-10;
    EYbeam = 2.42e-12;
    Nbun =    10000;
    NPar =   243000000000.0;
    HalfCrossingAngle = 0.015;


    Ebeam := sqrt( pbeam^2 + emass^2 );

    // Beam defined without radiation as a start - radiation is turned on later depending on the requirements
    BEAM, PARTICLE=ELECTRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

    USE, SEQUENCE = FCCEE_P_RING;
    VOLTCA1 = 0;
    VOLTCA2 = 0;

    SAVEBETA, LABEL=B.FG4, PLACE=FG.4, SEQUENCE=FCCEE_P_RING;
    SAVEBETA, LABEL=B.FFI1, PLACE=FFI.1, SEQUENCE=FCCEE_P_RING;
    SAVEBETA, LABEL=B.IP2, PLACE=IP.2, SEQUENCE=FCCEE_P_RING;
    SAVEBETA, LABEL=B.IP1, PLACE=IP.1, SEQUENCE=FCCEE_P_RING;

    TWISS;


    CALL, FILE = 'skew_quads.madx';

    SEQEDIT,SEQUENCE = FCCEE_P_RING;
    FLATTEN;

    EXTRACT, SEQUENCE = FCCEE_P_RING, FROM = IP.2,TO = FG.4, NEWNAME = IP_R;
    EXTRACT, SEQUENCE = FCCEE_P_RING, FROM = FFI.1,TO = IP.2, NEWNAME = IP_L;
    EXTRACT, SEQUENCE = FCCEE_P_RING, FROM = IP.1,TO = IP.8, NEWNAME = IP_L_R;
    !FLATTEN;
    ENDEDIT;

            

    ''')


    madx.input('val1=' + str(num1) + ';')
    madx.input('val2=' + str(num2) + ';')
    # madx.input('val3=' + str(num3) + ';')
    # madx.input('val4=' + str(num4) + ';')




               
    
    madx.input('''

    USE, SEQUENCE=IP_L;
    !Select, flag=twiss, column=name,s,betx,bety,alfx,alfy,dx,dpx,dy,dpy,r11,r12,r21,r22;
    !TWISS,BETA0=B.FFI1;

    !Match, USE_MACRO,BETA0=B.FFI1;
    Match, USE_MACRO,BETX=B.FFI1->BETX, BETY=B.FFI1->BETY, ALFX=B.FFI1->ALFX, ALFY=B.FFI1->ALFY, DX=B.FFI1->DX, DPX=B.FFI1->DPX;

    VARY, NAME= K_SF_0L;

    VARY, NAME= K_SY_1L;
    VARY, NAME= K_SY_2L;

    VARY, NAME= K_SF_3L;
    VARY, NAME= K_SF_4L;

    VARY, NAME= K_SD_5L;
    VARY, NAME= K_SD_6L;

    M1:MACRO={
    use, sequence=IP_L;
    select,flag=twiss,clear=true;
    select, flag=twiss,column=name,s,k1l,K1sl,k2l,x,y,betx,bety,dx,dy,alfx,alfy,mux,muy,R11,R12,R21,R22;          
    twiss,BETA0=B.FFI1,file="remove.tfs"; 
    system,"python run_script.py"; 
    call,file="ip_parameters.madx";

    };



    constraint,EXPR=diff_rdt_real=val1; !0.002214808289411328;
    constraint,EXPR=diff_rdt_imag=val2;
    constraint,EXPR=sum_rdt_real=0;   !0.0021520956291337743;
    constraint,EXPR=sum_rdt_imag=0;
    !constraint,EXPR=table(twiss,IP.2,BETX)=B.IP2->BETX;
    !constraint,EXPR=table(twiss,IP.2,BETY)=B.IP2->BETY;
    !constraint,EXPR=table(twiss,IP.2,DX)=B.IP2->DX;
    !constraint,EXPR=table(twiss,IP.2,DPX)=B.IP2->DPX;
    constraint,EXPR=table(twiss,IP.2,DY)=0;
    constraint,EXPR=table(twiss,IP.2,DPY)=0;





    JACOBIAN, CALLS=2000, TOLERANCE=1e-9;
    ENDMATCH;

    SAVEBETA,LABEL=B.IP2_COUPLING,PLACE=IP.2,SEQUENCE=IP_L;
    TWISS,BETA0=B.FFI1;
 




    !USE, SEQUENCE=IP_R;
    !SEQEDIT,SEQUENCE = IP_R;
    !FLATTEN;
    !REFLECT;
    !ENDEDIT;

    USE, SEQUENCE=IP_R;
    !Select, flag=twiss, column=name,s,l,betx,bety,alfx,alfy,dx,dpx,dy,dpy,r11,r12,r21,r22;
    




    MATCH, USE_MACRO,BETA0=B.IP2_COUPLING;
    VARY, NAME= K_SF_0R;

    VARY, NAME= K_SY_1R;
    VARY, NAME= K_SY_2R;
    VARY, NAME= K_SF_3R;
    VARY, NAME= K_SF_4R;
    VARY, NAME= K_SD_5R;

    VARY, NAME= K_SD_6R;
    


    M2:MACRO={
    use, sequence=IP_R;
    select,flag=twiss,clear=true;
    select, flag=twiss,column=name,s,k1l,K1sl,k2l,x,y,betx,bety,dx,dy,alfx,alfy,mux,muy,R11,R12,R21,R22;          
    twiss,BETA0=B.IP2_COUPLING,file="remove1.tfs"; 
    system,"python run_script1.py"; 
    call,file="ip_parameters1.madx";

    };

    !USE_MACRO,NAME=M2;

    constraint,EXPR=diff_rdt_real1=0; !0.002214808289411328;
    constraint,EXPR=diff_rdt_imag1=0;
    constraint,EXPR=sum_rdt_real1=0;   !0.0021520956291337743;
    constraint,EXPR=sum_rdt_imag1=0;
    !constraint,EXPR=table(twiss,FG.4,BETX)=B.IP2->BETX;
    !constraint,EXPR=table(twiss,FG.4,BETY)=B.IP2->BETY;
    !constraint,EXPR=table(twiss,FG.4,DX)=B.IP2->DX;
    !constraint,EXPR=table(twiss,FG.4,DPX)=B.IP2->DPX;
    constraint,EXPR=table(twiss,FG.4,DY)=0;
    constraint,EXPR=table(twiss,FG.4,DPY)=0;


    JACOBIAN, CALLS=2000, TOLERANCE=1e-9;

    ENDMATCH;



    !USE, SEQUENCE=IP_L_R;
    !select, flag=twiss, clear=True;
    !Select, flag=twiss, column=name,s,k1l,k2l,k1sl,l,betx,bety,alfx,alfy,dx,dpx,dy,dpy,r11,r12,r21,r22;
    !TWISS,BETA0=B.IP1,file="IP_L_R_1.tfs";

    USE,SEQUENCE=FCCEE_P_RING;
    select, flag=twiss, clear=True;
    Select, flag=twiss, column=name,keyword,s,k1l,k2l,k1sl,l,betx,bety,alfx,alfy,dx,dpx,dy,dpy,r11,r12,r21,r22;
    TWISS,FILE="COUPLING_remove.tfs";
    
    ''')





               
 
               


#### store the optics @ IP2 into a final data frame
def calculate_K1S(filename,df1,i):
    df_twiss = tfs.read(filename,index='NAME')
    df_twiss['NAMES']=df_twiss.index

    df_coupling = coupling_via_cmatrix(df_twiss)
    df_coupling['NAMES']=df_twiss.index
    df1['F1001_real'].iloc[i]=np.real(df_coupling['F1001'][df_coupling['NAMES']=='IP.2'])
    df1['F1001_imag'].iloc[i]=np.imag(df_coupling['F1001'][df_coupling['NAMES']=='IP.2'])
    df1['F1010_real'].iloc[i]=np.real(df_coupling['F1010'][df_coupling['NAMES']=='IP.2'])
    df1['F1010_imag'].iloc[i]=np.imag(df_coupling['F1010'][df_coupling['NAMES']=='IP.2'])
    df1['F1001'].iloc[i]=abs(df_coupling['F1001'][df_coupling['NAMES']=='IP.2'])
    df1['F1010'].iloc[i]=abs(df_coupling['F1010'][df_coupling['NAMES']=='IP.2'])




    df1['DY'].iloc[i]=df_twiss['DY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['DPY'].iloc[i]=df_twiss['DPY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['k1s_0l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.0L2')[0][0]]/(0.1)    #### uncomment when skewquad installed at final focus doublet
    df1['k1s_1l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.1L2')[0][0]]/(0.1)
    df1['k1s_2l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.2L2')[0][0]]/(0.1)
    df1['k1s_3l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.3L2')[0][0]]/(0.1)
    df1['k1s_4l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.4L2')[0][0]]/(0.1)
    df1['k1s_5l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.5L2')[0][0]]/(0.1)
    df1['k1s_6l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.6L2')[0][0]]/(0.1)

    df1['k1s_0r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.0R2')[0][0]]/(0.1)   #### uncomment when skewquad installed at final focus doublet
    df1['k1s_1r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.1R2')[0][0]]/(0.1)
    df1['k1s_2r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.2R2')[0][0]]/(0.1)
    df1['k1s_3r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.3R2')[0][0]]/(0.1)
    df1['k1s_4r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.4R2')[0][0]]/(0.1)
    df1['k1s_5r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.5R2')[0][0]]/(0.1)
    df1['k1s_6r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.6R2')[0][0]]/(0.1)

    df1['Qx'].iloc[i]=df_twiss.headers['Q1']
    df1['Qy'].iloc[i]=df_twiss.headers['Q2']
    df1['DQx'].iloc[i]=df_twiss.headers['DQ1']
    df1['DQy'].iloc[i]=df_twiss.headers['DQ2']

    df1['BETX'].iloc[i]=df_twiss['BETX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['BETY'].iloc[i]=df_twiss['BETY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['ALFX'].iloc[i]=df_twiss['ALFX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['ALFY'].iloc[i]=df_twiss['ALFY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['DX'].iloc[i]=df_twiss['DX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['DPX'].iloc[i]=df_twiss['DPX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]

    df1['R11'].iloc[i]=df_twiss['R11'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['R12'].iloc[i]=df_twiss['R12'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['R21'].iloc[i]=df_twiss['R21'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df1['R22'].iloc[i]=df_twiss['R22'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]



########### Provide knob_settings with in the range of application

# f1001_real=np.linspace(0,0.0005348,5)
# f1001_imag=np.linspace(0,0.000432,5)

# f1001_real=[0.0,5.94222222e-05, 1.18844444e-04, 1.78266667e-04, 2.37688889e-04, 2.97111111e-04, 3.56533333e-04, 4.15955556e-04,4.75377778e-04, 5.34800000e-04]
# f1001_imag=[0.0,4.80e-05, 9.60e-05, 1.44e-04, 1.92e-04, 2.40e-04,2.88e-04, 3.36e-04, 3.84e-04, 4.32e-04]

f1001_real=[0.0 , 0.00026444, 0.00052889, 0.00079333, 0.00105778,0.00132222, 0.00158667, 0.00185111, 0.00211556, 0.00238]
f1001_imag=[0.0, 0.00021333, 0.00042667, 0.00064   , 0.00085333,0.00106667, 0.00128   , 0.00149333, 0.00170667, 0.00192]



df_k1s=pd.DataFrame({'f1001_real':f1001_real})
df_k1s['f1001_imag']=f1001_imag
# df_k1s['f1001_real']=f1001_real
# df_k1s['f1001_imag']=f1001_imag
df_k1s['DY']=np.zeros(len(f1001_real))
df_k1s['DPY']=np.zeros(len(f1001_real))


df_k1s['F1001_real']=np.zeros(len(f1001_real))
df_k1s['F1001_imag']=np.zeros(len(f1001_real))
df_k1s['F1010_real']=np.zeros(len(f1001_real))
df_k1s['F1010_imag']=np.zeros(len(f1001_real))

df_k1s['F1001']=np.zeros(len(f1001_real))
df_k1s['F1010']=np.zeros(len(f1001_real))

df_k1s['k1s_0l']=np.zeros(len(f1001_real))    #### uncomment when skewquad installed at final focus doublet
df_k1s['k1s_1l']=np.zeros(len(f1001_real))
df_k1s['k1s_2l']=np.zeros(len(f1001_real))
df_k1s['k1s_3l']=np.zeros(len(f1001_real))
df_k1s['k1s_4l']=np.zeros(len(f1001_real))
df_k1s['k1s_5l']=np.zeros(len(f1001_real))
df_k1s['k1s_6l']=np.zeros(len(f1001_real))

df_k1s['k1s_0r']=np.zeros(len(f1001_real)) #### uncomment when skewquad installed at final focus doublet
df_k1s['k1s_1r']=np.zeros(len(f1001_real))
df_k1s['k1s_2r']=np.zeros(len(f1001_real))
df_k1s['k1s_3r']=np.zeros(len(f1001_real))
df_k1s['k1s_4r']=np.zeros(len(f1001_real))
df_k1s['k1s_5r']=np.zeros(len(f1001_real))
df_k1s['k1s_6r']=np.zeros(len(f1001_real))

df_k1s['Qx']=np.zeros(len(f1001_real))
df_k1s['Qy']=np.zeros(len(f1001_real))
df_k1s['DQx']=np.zeros(len(f1001_real))
df_k1s['DQy']=np.zeros(len(f1001_real))

df_k1s['BETX']=np.zeros(len(f1001_real))
df_k1s['BETY']=np.zeros(len(f1001_real))
df_k1s['ALFX']=np.zeros(len(f1001_real))
df_k1s['ALFY']=np.zeros(len(f1001_real))
df_k1s['DX']=np.zeros(len(f1001_real))
df_k1s['DPX']=np.zeros(len(f1001_real))

df_k1s['R11']=np.zeros(len(f1001_real))
df_k1s['R12']=np.zeros(len(f1001_real))
df_k1s['R21']=np.zeros(len(f1001_real))
df_k1s['R22']=np.zeros(len(f1001_real))

for val in range(0,len(f1001_real)):
    run_mad(f1001_real[val],f1001_imag[val])
    calculate_K1S('COUPLING_remove.tfs',df_k1s,val)
    os.remove('COUPLING_remove.tfs')

    
##### copy the final data frame to eos path provided where the data is plotted
df_k1s.to_csv('check_coupling_knobs_diff_rdt_full_twiss_skewquad_atfinaldoublet_highvalue.csv')
print(df_k1s)
madx = Madx(stdout=False)
madx.option(echo=True)
madx.input('system,"mv check_coupling_knobs_diff_rdt_full_twiss_skewquad_atfinaldoublet_highvalue.csv provide---your---eos----path ";')

































