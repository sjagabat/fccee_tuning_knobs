import logging
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import tfs
import sys
from cpymad.madx import Madx
from optics_functions.coupling import coupling_via_cmatrix, closest_tune_approach,coupling_via_rdts
from optics_functions.utils import split_complex_columns
from optics_functions.rdt import calculate_rdts, generator, jklm2str
from optics_functions.utils import prepare_twiss_dataframe, split_complex_columns

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")



############## Definition to run the madx and provide the final twiss in every iteration of knob values
def run_mad(disp,i):
    madx = Madx(stdout=True)
    madx.option(echo=True)
    madx.input('''

    option,update_from_parent=true; // new option in mad-x as of 2/2019


    !--------------------------------------------------------------
    ! Lattice selection and beam parameters
    !--------------------------------------------------------------

    CALL, FILE='Z_sequence.seq';


    pbeam =   45.6;
    EXbeam = 7.1e-20;
    EYbeam = 2.42e-22;
    Nbun =    10000;
    NPar =   243000000000.0;
    HalfCrossingAngle = 0.015;


    Ebeam := sqrt( pbeam^2 + emass^2 );

    // Beam defined without radiation as a start - radiation is turned on later depending on the requirements
    BEAM, PARTICLE=ELECTRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

    USE, SEQUENCE = FCCEE_P_RING;
    VOLTCA1 = 0;
    VOLTCA2 = 0;

    SAVEBETA, LABEL=B.FG4, PLACE=FG.4, SEQUENCE=FCCEE_P_RING;
    SAVEBETA, LABEL=B.FFI1, PLACE=FFI.1, SEQUENCE=FCCEE_P_RING;
    SAVEBETA, LABEL=B.IP2, PLACE=IP.2, SEQUENCE=FCCEE_P_RING;
    SAVEBETA, LABEL=B.IP1, PLACE=IP.1, SEQUENCE=FCCEE_P_RING;
   
    TWISS;

    CALL, FILE = 'skew_quads.madx';

    SEQEDIT,SEQUENCE = FCCEE_P_RING;
    FLATTEN;
    EXTRACT, SEQUENCE = FCCEE_P_RING, FROM = FFI.1,TO = IP.2, NEWNAME = IP_L;
    EXTRACT, SEQUENCE = FCCEE_P_RING, FROM = IP.2,TO = FG.4, NEWNAME = IP_R;
    EXTRACT, SEQUENCE = FCCEE_P_RING, FROM = FFI.1,TO = FG.4, NEWNAME = IP_L_R;

    !FLATTEN;
    ENDEDIT;
               



    ''')

    madx.input('IP_DISP=' + str(disp) + ';')
    madx.input('num=' + str(i) + ';')

 

    madx.input('''

    USE, SEQUENCE=IP_L;

    MATCH, SEQUENCE=IP_L, BETX=B.FFI1->BETX, BETY=B.FFI1->BETY, ALFX=B.FFI1->ALFX, ALFY=B.FFI1->ALFY, DX=B.FFI1->DX, DPX=B.FFI1->DPX;

    VARY, NAME= K_SF_0L;     !uncomment when you install skew quad at final focus doublet
    VARY, NAME= K_SY_1L;
    VARY, NAME= K_SY_2L;
    VARY, NAME= K_SF_3L;

    VARY, NAME= K_SF_4L;
    VARY, NAME= K_SD_5L;

    VARY, NAME= K_SD_6L;

    CONSTRAINT, RANGE=IP.2, DY = IP_DISP;
    CONSTRAINT, RANGE=IP.2, DPY = 0;
    
    CONSTRAINT, RANGE=IP.2, R11 = 0;
    CONSTRAINT, RANGE=IP.2, R12 = 0;
    CONSTRAINT, RANGE=IP.2, R21 = 0;
    CONSTRAINT, RANGE=IP.2, R22 = 0;



    JACOBIAN, CALLS=2000, TOLERANCE=1e-9;

    ENDMATCH;

    savebeta, label=B.IP2_DISP,place=IP.2,sequence=IP_L;
    !USE, SEQUENCE=IP_L;
    !Select, flag=twiss, column=name,s,k1l,k2l,k1sl,l,betx,bety,alfx,alfy,dx,dpx,dy,dpy,r11,r12,r21,r22;
    twiss, beta0=B.FFI1;
               

               
    USE, SEQUENCE=IP_R;
               
    
    !MATCH, SEQUENCE=IP_R, BETX=B.FG4->BETX, BETY=B.FG4->BETY, ALFX=-(B.FG4->ALFX), ALFY=-(B.FG4->ALFY), DX=B.FG4->DX, DPX=-(B.FG4->DPX);
    MATCH, SEQUENCE=IP_R, beta0=B.IP2_DISP;

    VARY, NAME= K_SF_0R;    !uncomment when you install skew quad at final focus doublet
    VARY, NAME= K_SY_1R;
    VARY, NAME= K_SY_2R;
    VARY, NAME= K_SF_3R;

    VARY, NAME= K_SF_4R;
    VARY, NAME= K_SD_5R;

    VARY, NAME= K_SD_6R;

    CONSTRAINT, RANGE=FG.4, DY = 0;
    CONSTRAINT, RANGE=FG.4, DPY = 0;
    
    CONSTRAINT, RANGE=FG.4, R11 = 0;
    CONSTRAINT, RANGE=FG.4, R12 = 0;
    CONSTRAINT, RANGE=FG.4, R21 = 0;
    CONSTRAINT, RANGE=FG.4, R22 = 0;


 

    JACOBIAN, CALLS=2000, TOLERANCE=1e-9;

    ENDMATCH;

    
          
    USE,SEQUENCE=FCCEE_P_RING;
    SELECT,FLAG=TWISS,CLEAR=TRUE;
    Select, flag=twiss, column=name,keyword,s,k1l,k2l,k1sl,l,betx,bety,alfx,alfy,dx,dpx,dy,dpy,r11,r12,r21,r22;
    TWISS,FILE="FULL_twiss.tfs";
               

               
    STOP;

    ''')


#### store the optics @ IP2 into a final data frame

def calculate_RDT_K1S(filename1,df,i):

    df_twiss = tfs.read(filename1,index='NAME')
    df_twiss['NAMES']=df_twiss.index
    df_coupling = coupling_via_cmatrix(df_twiss)
    df_coupling['NAME']=df_twiss.index
    df['DY'].iloc[i]=df_twiss['DY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['F1001'].iloc[i]=abs(df_coupling['F1001'][df_coupling['NAME']=='IP.2'])
    df['F1010'].iloc[i]=abs(df_coupling['F1010'][df_coupling['NAME']=='IP.2'])

    df['k1s_0l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.0L2')[0][0]]/(0.1)      ### uncomment when you install skew quad at final focus doublet
    df['k1s_1l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.1L2')[0][0]]/(0.1)
    df['k1s_2l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.2L2')[0][0]]/(0.1)
    df['k1s_3l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.3L2')[0][0]]/(0.1)
    df['k1s_4l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.4L2')[0][0]]/(0.1)
    df['k1s_5l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.5L2')[0][0]]/(0.1)
    df['k1s_6l'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.6L2')[0][0]]/(0.1)
    
    df['k1s_0r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.0R2')[0][0]]/(0.1)      ### uncomment when you install skew quad at final focus doublet
    df['k1s_1r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.1R2')[0][0]]/(0.1)
    df['k1s_2r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SY.2R2')[0][0]]/(0.1)
    df['k1s_3r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.3R2')[0][0]]/(0.1)
    df['k1s_4r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SF.4R2')[0][0]]/(0.1)
    df['k1s_5r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.5R2')[0][0]]/(0.1)
    df['k1s_6r'].iloc[i]=df_twiss['K1SL'][np.where(df_twiss['NAMES']=='Q_SD.6R2')[0][0]]/(0.1)

    df['Qx'].iloc[i]=df_twiss.headers['Q1']
    df['Qy'].iloc[i]=df_twiss.headers['Q2']
    df['DQx'].iloc[i]=df_twiss.headers['DQ1']
    df['DQy'].iloc[i]=df_twiss.headers['DQ2']

    df['BETX'].iloc[i]=df_twiss['BETX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['BETY'].iloc[i]=df_twiss['BETY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['ALFX'].iloc[i]=df_twiss['ALFX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['ALFY'].iloc[i]=df_twiss['ALFY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['DX'].iloc[i]=df_twiss['DX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['DPX'].iloc[i]=df_twiss['DPX'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['DPY'].iloc[i]=df_twiss['DPY'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]

    df['R11'].iloc[i]=df_twiss['R11'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['R12'].iloc[i]=df_twiss['R12'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['R21'].iloc[i]=df_twiss['R21'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]
    df['R22'].iloc[i]=df_twiss['R22'][np.where(df_twiss['NAMES']=='IP.2')[0][0]]

    


    



########### Provide knob_settings with in the range of application


disp_y=np.linspace(-1e-3,1e-3,25)

# disp_y=[0.1e-3]


df_rdt=pd.DataFrame({'Disp_Y':disp_y})



df_rdt['DY']=np.zeros(len(disp_y))
df_rdt['F1001']=np.zeros(len(disp_y))
df_rdt['F1010']=np.zeros(len(disp_y))

df_rdt['k1s_0l']=np.zeros(len(disp_y))   ### uncomment when you install skew quad at final focus doublet
df_rdt['k1s_1l']=np.zeros(len(disp_y))
df_rdt['k1s_2l']=np.zeros(len(disp_y))
df_rdt['k1s_3l']=np.zeros(len(disp_y))
df_rdt['k1s_4l']=np.zeros(len(disp_y))
df_rdt['k1s_5l']=np.zeros(len(disp_y))
df_rdt['k1s_6l']=np.zeros(len(disp_y))

df_rdt['k1s_0r']=np.zeros(len(disp_y))    ### uncomment when you install skew quad at final focus doublet
df_rdt['k1s_1r']=np.zeros(len(disp_y))
df_rdt['k1s_2r']=np.zeros(len(disp_y))
df_rdt['k1s_3r']=np.zeros(len(disp_y))
df_rdt['k1s_4r']=np.zeros(len(disp_y))
df_rdt['k1s_5r']=np.zeros(len(disp_y))
df_rdt['k1s_6r']=np.zeros(len(disp_y))

df_rdt['BETX']=np.zeros(len(disp_y))
df_rdt['BETY']=np.zeros(len(disp_y))
df_rdt['ALFX']=np.zeros(len(disp_y))
df_rdt['ALFY']=np.zeros(len(disp_y))
df_rdt['DX']=np.zeros(len(disp_y))
df_rdt['DPX']=np.zeros(len(disp_y))
df_rdt['DPY']=np.zeros(len(disp_y))

df_rdt['Qx']=np.zeros(len(disp_y))
df_rdt['Qy']=np.zeros(len(disp_y))
df_rdt['DQx']=np.zeros(len(disp_y))
df_rdt['DQy']=np.zeros(len(disp_y))

df_rdt['R11']=np.zeros(len(disp_y))
df_rdt['R12']=np.zeros(len(disp_y))
df_rdt['R21']=np.zeros(len(disp_y))
df_rdt['R22']=np.zeros(len(disp_y))


for val in range(0,len(disp_y)):

    run_mad(disp_y[val],val)
    calculate_RDT_K1S('FULL_twiss.tfs',df_rdt,val)
    os.remove('FULL_twiss.tfs')

    
    
##### copy the final data frame to eos path provided where the data is plotted
df_rdt.to_csv('vertical_dispersion_knob_1e-3_25values.csv')
print(df_rdt)
madx = Madx(stdout=False)
madx.option(echo=True)
madx.input('system,"mv vertical_dispersion_knob_1e-3_25values.csv  provide---your---eos----path ";')