import logging
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os.path
import os
import tfs
import sys
from cpymad.madx import Madx
from optics_functions.coupling import coupling_via_cmatrix,closest_tune_approach,coupling_via_rdts
from optics_functions.utils import split_complex_columns

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")


df_twiss1 = tfs.read('remove1.tfs',index='NAME')
df_coupling1 = coupling_via_cmatrix(df_twiss1)
df_coupling1['NAMES'] = df_twiss1.index
df_twiss1['NAMES'] = df_twiss1.index



diff_rdt_real1 = np.real(df_coupling1['F1001'][np.where(df_coupling1['NAMES']=='FG.4')[0][0]])
diff_rdt_imag1 = np.imag(df_coupling1['F1001'][np.where(df_coupling1['NAMES']=='FG.4')[0][0]])
sum_rdt_real1= np.real(df_coupling1['F1010'][np.where(df_coupling1['NAMES']=='FG.4')[0][0]])
sum_rdt_imag1 = np.imag(df_coupling1['F1010'][np.where(df_coupling1['NAMES']=='FG.4')[0][0]])



if os.path.exists('ip_parameters1.madx'):
    os.remove('ip_parameters1.madx')
    f = open("ip_parameters1.madx","w")

    f.write(f'diff_rdt_real1={diff_rdt_real1};\n')
    f.write(f'diff_rdt_imag1={diff_rdt_imag1};\n')
    f.write(f'sum_rdt_real1={sum_rdt_real1};\n')
    f.write(f'sum_rdt_imag1={sum_rdt_imag1};\n')

    f.close()
else:
    f = open("ip_parameters1.madx","w")

    f.write(f'diff_rdt_real1={diff_rdt_real1};\n')
    f.write(f'diff_rdt_imag1={diff_rdt_imag1};\n')
    f.write(f'sum_rdt_real1={sum_rdt_real1};\n')
    f.write(f'sum_rdt_imag1={sum_rdt_imag1};\n')
    f.close()


        



